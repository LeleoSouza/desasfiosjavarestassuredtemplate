package com.javarestassuredtemplate.requests.store;

import com.javarestassuredtemplate.bases.RequestRestBase;
import com.javarestassuredtemplate.utils.GeneralUtils;
import io.restassured.http.Method;

public class PostPlaceOrderRequest extends RequestRestBase {

    public PostPlaceOrderRequest(){
        method = Method.POST;
        requestService = "/store/order";
    }
    public void setJsonBodyUsingJsonFile(int orderId, int petId, int quantity){

        jsonBody = GeneralUtils.readFileToAString("src/test/java/com/javarestassuredtemplate/jsons/OrderPetJson.json")
                .replace("$orderId", String.valueOf(orderId))
                .replace("$petId", String.valueOf(petId))
                .replace("$quantity", String.valueOf(quantity));

    }
}
