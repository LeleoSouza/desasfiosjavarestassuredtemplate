package com.javarestassuredtemplate.requests.pet.deletePet;

import com.javarestassuredtemplate.bases.RequestRestBase;
import io.restassured.http.Method;

public class DeletePetRequest extends RequestRestBase {

    public void DeletePetRequest(int petId){
        requestService = "/pet/"+petId;
        method = Method.DELETE;
    }
}
