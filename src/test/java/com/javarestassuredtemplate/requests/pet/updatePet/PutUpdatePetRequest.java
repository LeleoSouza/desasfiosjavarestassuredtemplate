package com.javarestassuredtemplate.requests.pet.updatePet;

import com.javarestassuredtemplate.bases.RequestRestBase;
import com.javarestassuredtemplate.utils.GeneralUtils;
import io.restassured.http.Method;

public class PutUpdatePetRequest extends RequestRestBase {
    public PutUpdatePetRequest(){
        requestService = "/pet";
        method = Method.PUT;
    }
    public void setJsonBodyUsingJsonFile(int id,
                                         int categoryId,
                                         String categoryName,
                                         String newPetName,
                                         String photoUrl,
                                         int tagId,
                                         String tagName,
                                         String status) {
        jsonBody = GeneralUtils.readFileToAString("src/test/java/com/javarestassuredtemplate/jsons/PutUpdatePetJson.json")
                .replace("$id", String.valueOf(id))
                .replace("$categoryId", String.valueOf(categoryId))
                .replace("$categoryName", categoryName)
                .replace("$name", newPetName)
                .replace("$photoUrl", photoUrl)
                .replace("$tagId", String.valueOf(tagId))
                .replace("$tagName", tagName)
                .replace("$status", status);
    }
}

