package com.javarestassuredtemplate.requests.pet.getPetById;

import com.javarestassuredtemplate.bases.RequestRestBase;
import io.restassured.http.Method;

public class GetPetComMetodoErradoRequest extends RequestRestBase {
    public GetPetComMetodoErradoRequest(int petId){
        requestService = "/pet/"+petId;
        method = Method.PATCH;
    }
}
