package com.javarestassuredtemplate.requests.pet.getPetByStatus;

import com.javarestassuredtemplate.bases.RequestRestBase;
import io.restassured.http.Method;

public class GetPetByStatusRequest extends RequestRestBase {
    public GetPetByStatusRequest(String status){
        requestService = "/pet/findByStatus?status="+status;
        method = Method.GET;
    }
}
