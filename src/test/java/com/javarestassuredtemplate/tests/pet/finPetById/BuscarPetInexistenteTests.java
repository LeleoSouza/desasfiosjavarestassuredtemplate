package com.javarestassuredtemplate.tests.pet.finPetById;

import com.javarestassuredtemplate.bases.TestBase;
import com.javarestassuredtemplate.requests.pet.getPetById.GetPetRequest;
import com.javarestassuredtemplate.steps.DeletarPetSteps;
import io.restassured.response.ValidatableResponse;
import com.javarestassuredtemplate.steps.CadastrarPetSteps;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

public class BuscarPetInexistenteTests extends TestBase {
       //tinha um negocio aqui: GetPetRequest getPetRequest(roxo);

    @Test
    public void buscarPetInexistente(){
        //Parâmetros
        int petId = 555;
        int categoryId = 556;
        String categoryName = "Dogge";
        String petName = "Soluço";
        String photoUrl = "Imagina um link aqui e tal";
        int tagId = 555;
        String tagName = "Tag dos verdadeiros";
        String status = "avaiable";
        int statusCodeEsperado = HttpStatus.SC_NOT_FOUND;

        //Action
        int petIdReturn = CadastrarPetSteps.CadastrarPetSteps(petId,categoryId,categoryName,petName,
                                                                photoUrl, tagId, tagName, status);
        DeletarPetSteps.DeletarPetSteps(petIdReturn);
        GetPetRequest getPetRequest = new GetPetRequest(petIdReturn);
        ValidatableResponse response = getPetRequest.executeRequest();

        //Asserções
        response.statusCode(statusCodeEsperado);

    }
}
