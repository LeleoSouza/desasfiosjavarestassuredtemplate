package com.javarestassuredtemplate.tests.pet.finPetById;

import com.javarestassuredtemplate.bases.TestBase;
import com.javarestassuredtemplate.requests.pet.getPetById.GetPetComMetodoErradoRequest;
import com.javarestassuredtemplate.steps.CadastrarPetSteps;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

public class BuscarPetExistenteComMetodoErrado extends TestBase {
       //tinha um negocio aqui: GetPetRequest getPetRequest(roxo);

    @Test
    public void BuscarPetExistenteComMetodoErrado(){
        //Parâmetros
        int petId = 555;
        int categoryId = 556;
        String categoryName = "Dogge";
        String petName = "Soluço";
        String photoUrl = "Imagina um link aqui e tal";
        int tagId = 555;
        String tagName = "Tag dos verdadeiros";
        String status = "pending";
        int statusCodeEsperado = HttpStatus.SC_METHOD_NOT_ALLOWED;

        //Action
        int petIdReturn = CadastrarPetSteps.CadastrarPetSteps(petId,categoryId,categoryName,petName,
                                                                photoUrl, tagId, tagName, status);
        GetPetComMetodoErradoRequest getPetComMetodoErradoRequest = new GetPetComMetodoErradoRequest(petIdReturn);
        ValidatableResponse response = getPetComMetodoErradoRequest.executeRequest();

        //Asserções
        response.statusCode(statusCodeEsperado);

    }
}
