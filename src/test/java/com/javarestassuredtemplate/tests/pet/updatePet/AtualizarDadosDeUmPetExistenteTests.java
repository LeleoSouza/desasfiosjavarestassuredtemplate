package com.javarestassuredtemplate.tests.pet.updatePet;

import com.javarestassuredtemplate.bases.TestBase;
import com.javarestassuredtemplate.requests.pet.updatePet.PutUpdatePetRequest;
import com.javarestassuredtemplate.steps.CadastrarPetSteps;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import static org.hamcrest.Matchers.equalTo;

public class AtualizarDadosDeUmPetExistenteTests extends TestBase {

    @Test
    public void AtualizarDadosDeUmPetExistenteTests(){

        int petId = 555;
        int categoryId = 556;
        String categoryName = "Dogge";
        String petName = "Soluço";
        String newpetName = "Soluço Pera";
        String photoUrl = "Imagina um link aqui e tal";
        int tagId = 555;
        String tagName = "Tag dos verdadeiros";
        String status = "avaiable";
        int statusCodeEsperado = HttpStatus.SC_OK;


        int petIdReturn = CadastrarPetSteps.CadastrarPetSteps(petId, categoryId, categoryName, petName, photoUrl,
                                                                tagId,tagName, status);
        PutUpdatePetRequest putUpdatePetRequest = new PutUpdatePetRequest();
        putUpdatePetRequest.setJsonBodyUsingJsonFile(petIdReturn, categoryId, categoryName, newpetName,
                                                        photoUrl,tagId,tagName,status);
        ValidatableResponse response = putUpdatePetRequest.executeRequest();

        response.statusCode(statusCodeEsperado);
        response.body("id", equalTo(petIdReturn));
        response.body("name", equalTo(newpetName));


    }

}
