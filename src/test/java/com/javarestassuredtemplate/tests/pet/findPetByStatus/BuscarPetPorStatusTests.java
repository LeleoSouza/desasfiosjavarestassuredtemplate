package com.javarestassuredtemplate.tests.pet.findPetByStatus;

import com.javarestassuredtemplate.bases.TestBase;
import com.javarestassuredtemplate.requests.pet.getPetByStatus.GetPetByStatusRequest;
import com.javarestassuredtemplate.steps.CadastrarPetSteps;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

public class BuscarPetPorStatusTests extends TestBase {
       //tinha um negocio aqui: GetPetRequest getPetRequest(roxo);

    @Test
    public void BuscarPetPorStatus(){
        //Parâmetros
        int petId = 555;
        int categoryId = 556;
        String categoryName = "Dogge";
        String petName = "Soluço";
        String photoUrl = "Imagina um link aqui e tal";
        int tagId = 555;
        String tagName = "Tag dos verdadeiros";
        String status = "pending";
        int statusCodeEsperado = HttpStatus.SC_OK;

        //Action
        CadastrarPetSteps.CadastrarPetSteps(petId,categoryId,categoryName,petName,
                                                                photoUrl, tagId, tagName, status);
        GetPetByStatusRequest getPetByStatusRequest = new GetPetByStatusRequest(status);
        ValidatableResponse response = getPetByStatusRequest.executeRequest();

        //Asserções
        response.statusCode(statusCodeEsperado);

    }
}
