package com.javarestassuredtemplate.tests.store.placeOrder;

import com.javarestassuredtemplate.bases.TestBase;
import com.javarestassuredtemplate.requests.store.PostPlaceOrderRequest;
import com.javarestassuredtemplate.steps.CadastrarPetSteps;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import static org.hamcrest.Matchers.equalTo;

public class PostPlaceOrderTests extends TestBase {

    @Test
    public void ComprarPetComSucesso(){

        //arrange
        int petId = 555;
        int categoryId = 556;
        String categoryName = "Dogge";
        String petName = "Soluço";
        String photoUrl = "Imagina um link aqui e tal";
        int tagId = 555;
        String tagName = "Tag dos verdadeiros";
        String status = "avaiable";

        int orderID = 557;
        int quantity = 1;
        int statusCodeEsperado = HttpStatus.SC_OK;

        //actions
        int petIdReturn = CadastrarPetSteps.CadastrarPetSteps(petId, categoryId, categoryName, petName, photoUrl, tagId,
                                                    tagName, status);

        PostPlaceOrderRequest placeOrderRequest = new PostPlaceOrderRequest();
        placeOrderRequest.setJsonBodyUsingJsonFile(orderID, petIdReturn, quantity);
        ValidatableResponse response = placeOrderRequest.executeRequest();

        //assert
        response.statusCode(statusCodeEsperado);
        response.body("petId", equalTo(petIdReturn));
        response.body("quantity", equalTo(quantity));

    }
}
