package com.javarestassuredtemplate.steps;

import com.javarestassuredtemplate.requests.pet.deletePet.DeletePetRequest;

public class DeletarPetSteps {

    public static void DeletarPetSteps(int petId){

        DeletePetRequest deletePetRequest = new DeletePetRequest();
        deletePetRequest.DeletePetRequest(petId);
        deletePetRequest.executeRequest();

    }
}
