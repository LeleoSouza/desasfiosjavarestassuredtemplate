package com.javarestassuredtemplate.steps;

import com.javarestassuredtemplate.requests.pet.createPet.PostCreatePetRequest;

public class CadastrarPetSteps {
    public static int CadastrarPetSteps(int petId, int categoryId, String categoryName,
                                        String petName, String photo, int tagId, String tagName, String status){

        PostCreatePetRequest postCreatePetRequest = new PostCreatePetRequest();
        postCreatePetRequest.setJsonBodyUsingJsonFile(petId, categoryId, categoryName,
                                                        petName, photo, tagId, tagName, status);
        postCreatePetRequest.executeRequest();
        return petId;


    }
}
